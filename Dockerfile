# Use the official Rust image as the base image
FROM rust:latest

# Create a new directory for the application
WORKDIR /app

# Copy the Cargo.toml and Cargo.lock files to the container
COPY Cargo.toml Cargo.lock ./

# Build the dependencies without the application code
RUN cargo build --release

# Copy the source code to the container
COPY src ./src

# Build the application
RUN cargo build --release

# Set the entrypoint to run the application
CMD ["./target/release/rust_kafka"]