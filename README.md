# Rust-kafka

A small sample project for setting up a Kafka consumer/producer in Rust.

This is mostly a PoC so I can expand on it later on. Currently it does nothing and was largely taken from the Rust rdkafka docs.
