use std::env;
use std::thread;

use serde::{Deserialize, Serialize};

use rdkafka::config::ClientConfig;
use rdkafka::consumer::BaseConsumer;
use rdkafka::consumer::Consumer;
use rdkafka::producer::{BaseProducer, BaseRecord};
use rdkafka::message::Message;

use serde_json;

#[derive(Debug, Deserialize, Serialize)]
struct User {
    name: String,
    email: String,
    age: i32,
    active: bool,
}

#[tokio::main]
async fn main() {
    let topics = env::var("TOPICS").unwrap_or_else(|_| "rust".to_owned());
    let brokers = env::var("BROKERS").unwrap_or_else(|_| "localhost:9092".to_owned());
    let group_id = env::var("GROUP_ID").unwrap_or_else(|_| "my_consumer_group".to_owned());
    let username = env::var("USERNAME").unwrap_or_else(|_| "admin".to_owned());
    let password = env::var("PASSWORD").unwrap_or_else(|_| "the_knights_who_say_ni".to_owned());

    let producer: BaseProducer = ClientConfig::new()
        .set("bootstrap.servers", brokers.clone())
        .set("security.protocol", "SASL_SSL")
        .set("sasl.mechanisms", "PLAIN")
        .set("sasl.username",username)
        .set("sasl.password", password)
        .create()
        .expect("Invalid producer config");

    let consumer: BaseConsumer = ClientConfig::new()
        .set("bootstrap.servers", brokers.clone())
        .set("group.id", group_id)
        .create()
        .expect("Invalid consumer config");

    consumer
        .subscribe(&[topics.as_str()])
        .expect("topic subscribe failed");

    thread::spawn(move || loop {
        for msg_result in consumer.iter() {
            let msg = msg_result.unwrap();
            let key: &str = msg.key_view().unwrap().unwrap();
            let value = msg.payload().unwrap();
            let user: User = serde_json::from_slice(value).expect("Failed to deserialize JSON to User");
            println!(
                "Received key {} with value {:?} in offset {:?} from partition {}",
                key,
                user,
                msg.offset(),
                msg.partition()
            );
            let user_record = serde_json::to_vec(&user).expect("Failed to serialize User to JSON");
            let record = BaseRecord::to(&topics).key("user").payload(&user_record);
            producer.send(record).expect("Failed to send message");
        }
    });
}